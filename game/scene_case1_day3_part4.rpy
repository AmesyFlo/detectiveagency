label case1_day3_part4:
    show bg office evening with Dissolve(1.0)
    $ audio_crossFade(1.5, "audio/brunch.mp3")
    "I paced the office, watching the phone as if expecting Louise to ring any minute."
    "If she could get the usb he was finished."
    "Fern was spinning restlessly in her chair."
    show fern_sad with Dissolve(0.25)
    f sad "I'm bored!"
    hide fern_sad
    show fern_happy
    f happy "I know, let's play never have I ever. I'll go first."
    f happy "Never have I ever lied about the fact that my ex-roommate is heading up the local police station!"
    f happy "I think you'd better drink, Jan."
    j annoyed "Really? Right now? You want to do this?"
    hide fern_happy
    show fern_base
    f base "Yes. I want you to stop hiding things from me."
    f base "I'm your friend and I'm worried and I'm a goddammned hacker so hiding things from me is pointless."
    f base "At the very least, this is going to affect our operations."
    j annoyed "Look, if I agree to tell you after the case, will you get off my back?"
    play sound door
    show lou_happy with Dissolve(0.25)
    hide fern_base with Dissolve(0.25)
    show fern_base at left with Dissolve(0.25)
    "Our argument was interrupted by the arrival of Louise, once again bursting into the office in a rush."
    "This was becoming a habit."

    l happy "I got it!"
    hide fern_base
    show fern_happy at left
    f happy "Lets take a look at this sucker."
    hide fern_happy
    show fern_proud at left
    f proud "This right here is Halotus 3.0. My dummy machine for plugging weird shit into."
    hide fern_proud
    show fern_thinking at left
    f thinking "Nothing that looks malicious so far...lets open up some of these docs."
    hide fern_thinking
    show fern_base at left
    f base "Doesn't mean anything to me."
    hide lou_happy
    show lou_sad
    l sad "Oh, no."
    j thinking "Do you know what these are, Louise?"
    l sad "These records...they're highly confidential. They're detailed finance plans."
    l sad "Where did these come from?"
    j thinking "The documents with the usb, they were receipts. He paid for this."
    l sad "...his promotion. No."
    l sad "I had wondered how he'd been promoted so fast, he was never that good at his job to be honest."
    hide lou_sad
    show lou_angry
    l angry "Fuck! He's been buying and selling information! This is corporate espionage!"
    l angry "This is a million times worse than cheating."
    hide lou_angry
    show lou_sad
    l sad "I can't, I can't be connected to this, it would ruin me!"
    l sad "What...do I do?"

    j thinking "Well, we can hand this over to the police, or we can alert his bosses."
    j thinking "I'm not sure if there's much else to be done."
    hide lou_sad with Dissolve(0.25)
    hide fern_base with Dissolve(0.25)

    menu:
        "I'm going to call the police.":
            show lou_sad with Dissolve(0.25)
            show fern_base at left with Dissolve(0.25)
            j thinking "I'm going to call the police."
            j thinking "After what I did to get this info, I need a bit of goodwill with them."
            j thinking "We can't not report stuff like this. I'm sorry."
            j thinking "I'll still provide the full report for you to look over."
            j thinking "If there's anything else we can do, we're a phone call away."

            l sad "I understand. I'd report it in your situation too."
            l sad "But if I'm not out in front of this I'm going to look like an accomplice."
            j thinking "It was you who got us involved in the first place, so I wouldn't worry."
            j thinking "But if you want to be the one to make the report, I'll be behind you."

            hide lou_sad
            show lou_base
            l base "I'd like that, thanks."

        "Its your decision.":
            show lou_sad with Dissolve(0.25)
            show fern_base at left with Dissolve(0.25)
            j thinking "I'll let you decide what to do."
            j thinking "I'd recommend you call the police though."
            j thinking "I'll still provide the full report for you to look over."
            j thinking "If there's anything else we can do, we're a phone call away."

            l sad "If I'm not out in front of this I'm going to look like an accomplice."
            l sad "What he's doing is wrong."
            hide lou_sad
            show lou_angry
            l angry "Its screwing over the rest of us who are just trying to do our jobs!"
            l angry "We're pulling the weight while he's playing spy!"
            hide lou_angry
            show lou_sad
            l sad "That's not the man I married."
            l sad "What happened to him?"

            j thinking "I really am sorry Louise."
            j thinking "I can help you make the report if you'd like?"
            hide lou_sad
            show lou_base
            l base "Yeah, that would be good, thanks."
            hide lou_base with Dissolve(0.25)
            hide fern_base with Dissolve(0.25)

    stop music fadeout 3.0
    $ audio_crossFade(1.5, "audio/lovin.mp3")

    "And so our the not-so-simple infidelity case came to an end."
    "We helped Louise phone the station and sent the documents over, in the guise of an anonymous P.I."
    "They will have known exactly who it was, but I was trying to keep my name as far away from Avery Jones as possible."
    "We'll just have to wait and see what happens to Trevor, though my gut says some slippery lawyer behind closed doors will see him walk away scot free."
    "I just hope that Louise gets her divorce and her promotion."

    hide bg office evening with Dissolve(1.0)
