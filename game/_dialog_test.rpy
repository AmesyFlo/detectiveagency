label dialog_test:
    stop music
    stop music1
    show bg office afternoon with dissolve
    show fern_happy with Dissolve(0.25)

    f happy "This is a test of the dialog system."
    hide fern_happy
    show fern_thinking
    f thinking "Testing"
    hide fern_thinking
    show fern_happy
    f happy "Testing"
    hide fern_happy
    show fern_thinking
    f thinking "1...2...3"
