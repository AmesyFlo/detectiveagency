label case1_day2_part2:
    show bg warehouse with Dissolve(1.0)
    $ audio_crossFade(1.5, "audio/spy.mp3")
    "I pulled up to the warehouse."
    "It looked abandoned. There was a broken down minibus sitting outside the dilapidated building."
    "I reminded myself to look up the registration later."
    $ case_dict["Bus"] = "- Bus registration is BD5I SMR."
    "I took a look around, but couldn't find a window or unlocked door."
    "In the end I resorted to one of my least favorite tricks - rifling through the trash."
    "This address was listed on the invoice, so mail must come through here."
    "I found some large industrial bins. Great."
    "Thankfully, it seemed to be mostly paper inside."
    "The majority was shredded, but there were some larger pieces here and there that hadn't gone through properly."
    "It was difficult to decipher, but I kept seeing the same two names pop up over and over."
    show document names
    "R.Smith and A.Veroni."
    "Could one of these be our mystery man?"
    $ case_dict["Smith"] = "- R.Smith and A.Veroni are connected to the warehouse."

    j proud "Hey. Can you look up R.Smith and A.Veroni for me? Looks like they're connected with this building."
    f base "Two secs."
    f base "There's a hundred R.Smith' just in Northpool Green, I can check them against the picture, but it'll take some time."
    f base "I've found some A.Veronis in Italy...nothing here though. Any other info I can go on?"
    j thinking "Not yet. I'll keep looking."

    "I got nothing more out of the search of the warehouse area, but I did have an idea of how to connect these names to Trevor."
    "I headed back to the office to test my theory, but was sidetracked but more important news."
    hide document names

    hide bg warehouse with Dissolve(1.0)
    stop music fadeout 3.0
    show bg office evening with Dissolve(1.0)
    $ audio_crossFade(1.5, "audio/brunch.mp3")
    show fern_sad with Dissolve(0.25)
    f sad "Hey. I figured out why the sarge hasn't called."
    j thinking "Oh? Finally decided to ditch us for someone who knows what they're doing?"
    hide fern_sad
    show fern_base
    f base "Nope. He retired."
    hide fern_base
    show fern_angry
    f angry "Without telling us!"
    hide fern_angry
    show fern_sad
    f sad "I called earlier and the receptionist was all 'oh you didn't know?'. Ugh."
    f sad "There goes our most interesting cases."
    j annoyed "I'm not surprised to be honest. He was one of the few good folk there."
    j annoyed "With all the old guard gone, those crooks probably pushed him out."
    j annoyed "Anyone with an inch of a conscience won't last long on the force."
    hide fern_sad
    show fern_thinking
    f thinking "Was that why you left?"
    j angry "You could put it that way, though left is a kind choice of words!"
    j annoyed "Fired more like."
    hide fern_thinking
    show fern_proud
    f proud "Is this where you FINALLY tell me the story of January and her police days?"
    j proud "No, it is not!"
    j thinking "You didn't happen to find out who's in charge at the station now did you?"
    hide fern_proud
    show fern_thinking
    f thinking "Someone called Avery Jones? The guy on the desk didn't seem to be a fan to be honest."
    j annoyed "..."
    j annoyed "Fuck."
    j annoyed "I need to go."
    hide fern_thinking
    show fern_angry
    f angry "Wait what? You can't just have that reaction and go! Who are they?!"
    f angry "ONE day you're going to tell me the story of you being a cop and what your connection to this Jones person is!"
    j annoyed "I don't want to dredge up the past Fern. Its all the SAME story, and its not a good one."
    hide fern_angry with Dissolve(0.25)

    play sound door
    hide bg office evening with Dissolve(1.0)

    "I left her puzzling there as I left the office to do some more reconnaissance work."
    "It didn't feel good, avoiding questions and pawning Fern off, but I started the agency to get away from that life."
    "To get away from HER."

    $ health.reset_health()
