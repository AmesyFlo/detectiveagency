label tutorial:

    show bg office afternoon with Dissolve(0.5)

    $ audio_crossFade(1.5, "audio/office.mp3")
    show fern_base with Dissolve(0.25)
    f base "Ooh, rough night?"
    j angry "Thanks, good morning to you too Fern!"

    hide fern_base
    show fern_thinking
    f thinking "Hah, sorry. Just, are you sure you should be at work? You don't look well."
    hide fern_thinking
    show fern_sad
    j "I'm fine."
    hide fern_sad
    show fern_happy
    f happy "Okay, prove it to me!"
    j "What?"
    f happy" Show me those detective skills of yours and I'll know you're fit for work."
    j "Need I remind you who's the boss in this situation?"
    hide fern_happy
    show fern_sad
    f sad "Jan! C'mon!"
    j "Fine, I'll play your game. Hit me."

    stop music fadeout 3.0
    $ audio_crossFade(1.5, "audio/mystery.mp3")
    hide fern_sad
    show fern_thinking
    f thinking "On a train, Smith, Robinson, and Jones are the fireman, brakeman, and the engineer, but not necessarily respectively. Also aboard the train are three businessmen who have the same names: a Mr. Smith, a Mr. Robinson, and a Mr. Jones."
    f thinking "Mr. Robinson lives in Detroit and the brakeman lives exactly halfway between Chicago and Detroit."
    f thinking "Mr. Jones earns exactly $20,000 per year. The brakeman's nearest neighbor, one of the passengers, earns exactly three times as much as the brakeman."
    f thinking "Smith beats the fireman in billiards. The passenger whose name is the same as the brakeman's lives in Chicago."
    f thinking "What is the engineer's name?"

    j "Woah, too much info all at once! None of that had anything to do with the engineer!"

    hide fern_thinking
    show fern_angry
    f angry "Write it down then! I'll go slower this time."
    hide fern_angry
    show fern_base
    f base "Mr. Robinson lives in Detroit."
    $ case_dict["Detroit"] = "- Mr. Robinson lives in Detroit."
    f base "The brakeman lives exactly halfway between Chicago and Detroit."
    $ case_dict["Halfway"] = "- Brakeman lives halfway between Chicago and Detroit."
    f base "Mr. Jones earns exactly $20,000 per year."
    $ case_dict["Earnings"] = "- Mr. Jones earns exactly $20,000 per year."
    f base "The brakeman's nearest neighbor, one of the passengers, earns exactly three times as much as the brakeman."
    $ case_dict["Three"] = "- A passenger is the brakeman's neighbor and earns three times as much as the brakeman."
    f base "Smith beats the fireman in billiards."
    $ case_dict["Billiards"] = "- Smith beat the fireman in billiards."
    f base "The passenger whose name is the same as the brakeman's lives in Chicago."
    $ case_dict["Chicago"] = "- The passenger whose name is the same as the brakeman's lives in Chicago."

    j "Okay, so we need to do this in stages. First I need to get rid of any gaps in the information."
    j "I need to do it quickly too. If I get this wrong we'll be waste our day playing this stupid game. Leads dry up if you don't get to them on time!"
    j "I can click on my phone to swap between the time and my case notes."
    hide screen case_notes
    j "You mentioned a nearest neighbor, but not a name."
    hide fern_base with Dissolve(0.25)
    label .location:
        $ health.set_preview(1)
        menu:
            j "They live halfway between Chicago and Detroit, so it isn't"
            "Mr. Robinson":
                $ health.hide_preview()

            "Mr. Smith":
                $ health.fail_tutorial("tutorial.location")

            "Mr. Jones":
                $ health.fail_tutorial("tutorial.location")

    show fern_base with Dissolve(0.25)
    j "Mr. Robinson lives in Detroit, so it isn't him."
    hide fern_base with Dissolve(0.25)

    label .earnings:
        $ health.set_preview(1)
        menu:
            j "They also earn exactly three times as much as the brakeman. That means"
            "It's not Mr. Jones":
                $ health.hide_preview()

            "It is Mr. Jones":
                $ health.fail_tutorial("tutorial.earnings")

    show fern_base with Dissolve(0.25)
    j "The neighbor is Mr.Smith. Mr. Jones earns $20,000, which can't be three times as much as the brakeman's salary - it isn't divisible by three."

    $ del case_dict["Three"]
    $ case_dict["Neighbor"] = "- The breakman's neighbor is Mr. Smith and he earns three times as much as the brakeman."
    hide fern_base with Dissolve(0.25)

    label .relevant:
        $ health.set_preview(1)
        menu:
            j "Next, remove any information that isn't useful. Is Smith beating the fireman at billiards relevant to the answer?"
            "Yes":
                $ health.hide_preview()

            "No":
                $ health.fail_tutorial("tutorial.relevant")

    show fern_base with Dissolve(0.25)
    j "Ah, it is relevant - it's Smith, not Mr.Smith, so Smith can't be the fireman."
    $ del case_dict["Billiards"]
    $ case_dict["Smith"] = "- Smith is not the fireman."
    hide fern_base with Dissolve(0.25)

    label .breakman:
        $ health.set_preview(1)
        menu:
            j "Okay, the passenger whose name is the same as the brakeman's lives in Chicago. Who's name is the same as the brakeman's?"
            "Mr. Robinson":
                $ health.fail_tutorial("tutorial.breakman")

            "Mr. Smith":
                $ health.fail_tutorial("tutorial.breakman")

            "Mr. Jones":
                $ health.hide_preview()

    show fern_base with Dissolve(0.25)
    j "Mr. Robinson lives in Detroit and Mr. Smith lives between the two cities, so it has to be Mr. Jones that shares the same name as the brakeman. "
    $ case_dict["Smith"] = "- Jones is the brakeman."
    hide fern_base with Dissolve(0.25)

    label.engineer:
        $ health.set_preview(1)
        menu:
            j "Jones is the brakeman and Smith is not the fireman, so the engineer has to be"
            "Mr. Robinson":
                $ health.fail_tutorial("tutorial.engineer")

            "Mr. Smith":
                $ health.hide_preview()

            "Mr. Jones":
                $ health.fail_tutorial("tutorial.engineer")

    show fern_proud with Dissolve(0.25)
    $ audio_crossFade(1.5, "audio/office.mp3")
    f proud "I take it all back. Wonderful work detective."

    python:
        for k,v in case_dict.items():
            del case_dict[k]

    f proud "Now are you ready for some real work?"
    j "...I would be if we had any cases."

    $ health.reset_health()
    hide fern_proud
    hide bg office afternoon

    return

label tutorial_fail:
    show fern_sad with Dissolve(0.25)
    f "Mmm, don't think so Jan!"
    f "Go home and get some rest."
    f "Our lack of cases can wait 'til tomorrow."
    "GAME OVER"
    $ MainMenu(confirm=False)()
