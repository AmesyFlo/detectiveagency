label case1_day3_part1:
    $ audio_crossFade(1.5, "audio/office.mp3")
    show bg office afternoon with Dissolve(1.0)

    show fern_sad with Dissolve(0.25)
    j happy "Morning!"
    f sad "Hi."
    j thinking "Look, I'm sorry I left in such a hurry last night."
    j thinking "If its any consolation, I rang the Billford?"
    j thinking "Managed to do my 'corporate voice' and get them to dish the info."
    j thinking "R.Smith stayed there, but A.Veroni didn't."
    j thinking "So Smith's our guy."
    $ case_dict["Smith"] = "- R.Smith is connected to the warehouse and stayed at the Billford Hotel."

    hide fern_sad
    show fern_happy
    f happy "Good to know! Lets get 'im!"
    "I knew Fern was mad at me and faking her enthusiasm. But what was I supposed to do?"
    "Face all of...that? I don't think so."

    hide fern_happy
    show fern_thinking
    f thinking "Oh, I checked companies house."
    f thinking "Our A.Veroni is listed as the director of KnowLedger. First name Araminta."
    f thinking "You'd think a name like that would be an easy pick on social, but there was nothing there."
    f thinking "A ghost. Initially erased her presence from the net."
    f vhappy "So suspicious!"

    j thinking "A company who won't list what they're really paying for, whose CEO is an online ghost."
    j proud "Totally legit then yeah?"

    hide fern_thinking
    show fern_sad
    f sad "As exciting as it is, we won't get a lot from her. I think we need to focus on Smith."
    j thinking "Well, you're the - what was it - 'hacker extraordinaire'? I'll trust your gut on this one."

    hide fern_sad
    show fern_proud
    f proud "Flattery will get you everywhere, my dear January."
    f proud "Let me take a look..."
