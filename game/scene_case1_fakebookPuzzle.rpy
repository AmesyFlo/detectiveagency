label fakebook_puzzle:

    $ case_dict["Message"] = "- Trevor was asked to meet a 'non-descript' man at the hotel."
    $ case_dict["Name"] = "- We're pretty sure the man he met with was called R.Smith."
    $ case_dict["Description"] = "- We saw a young, red haired guy in a tracksuit leave the hotel."

    $ audio_crossFade(1.5, "audio/mystery.mp3")

    show bg office afternoon with Dissolve(1.0)
    show fern_angry with Dissolve(0.25)

    f angry "There's dozens of R. Smith's just in Northpool Green alone!"
    hide fern_angry
    show fern_sad
    f sad "It'll take me ages to sort through all this..."
    j proud "Don't worry. I think we can narrow it down a bit."

label .question_1:
    $ health.set_preview(1)
    menu:
        j "Lets's start by eliminating profiles by"

        "Hair Color":
            j thinking "their hair color."
            j thinking "That guy had red hair right? So we can just look for red haired people."
            f sad "I'll have to look at every picture though...that will take hours."
            f sad "And what if he has an anime girl as his profile picture?"
            j annoyed "People do that?"
            hide fern_sad
            show fern_angry
            f angry "Yes! My profile pic is of Max Spirit! She's a spirit medium who..."
            hide screen case_notes
            "Fern started going on about her game. We ended up wasting most of the day comparing the characters to our own office."
            "At least she was talking to me after last night."
            $ health.fail("fakebook_puzzle.question_1")

        "Gender":
            $ health.hide_preview()
            j thinking "their gender."
            j thinking "We target was described as male, and Fakebook forces you to list a gender on your profile."
            j thinking "So only keep the profiles that have their gender listed as male."
            hide fern_sad
            show fern_happy
            f happy "Got it boss!"
            hide fern_happy
            show fern_thinking

        "Relationship Status":
            j thinking "their relationship status."
            f thinking "Err Jan? What's that got to do with anything?"
            hide screen case_notes
            j annoyed "Mmm, maybe it doesn't have anything to do with the target."
            $ health.fail("fakebook_puzzle.question_1")

label .question_2:
    $ health.set_preview(1)
    menu:
        j "Okay, now we can thin the men out by"

        "Birthday":
            $ health.hide_preview()
            j thinking "their birthday."
            j thinking "The guy was pretty young looking right? So we can narrow the birth dates down."
            j thinking "Birth dates between '86 and 2002 should do the trick. That would make them between eighteen and thirty-five."

        "Profile Picture":
            hide fern_thinking
            show fern_sad
            f sad "I'll have to look at every picture though...its less than before but it'll still take hours!"
            f sad "And what if he has an anime girl as his profile picture?"
            j annoyed "People do that?"
            hide fern_sad
            show fern_angry
            f angry "Yes! My profile pic is of Max Spirit! She's a spirit medium who..."
            hide screen case_notes
            "Fern started going on about her game. We ended up wasting most of the day comparing the characters to our own office."
            "At least she was talking to me after last night."
            $ health.fail("fakebook_puzzle.question_2")

        "Interests":
            j thinking "their interests."
            f thinking "What do their interests have to do with narrowing down the target?"
            j annoyed "Maybe one of them put that he's interested in crime? Hah."
            hide screen case_notes
            j annoyed "Scrap that. Lets look elsewhere."
            $ health.fail("fakebook_puzzle.question_2")

    hide fern_thinking
    show fern_happy
    f happy "Right, young men, called R. Smith, in the area. Oh! We're down to three! Nice one Jan!"
    hide fern_happy
    show fern_thinking
    f thinking "What now?"
    j proud "We're on the right track now. We need to look closer to figure out who these guys really are."

    label .look_at:
        menu:
            j "I want to look at"

            "Tagged Locations":
                f thinking "Looks like Richard and Ralph are security conscious - they have tagged locations turned off."
                f thinking "Ross though, he's been tagged at the arena, he was at a gig recently."
                hide fern_thinking
                show fern_happy
                f happy "Oh! Its Rainy West! He even got a selfie with Derek, jelly!"
                j happy "Me too. I used to go see Rainy West with my old flatmate."
                hide fern_happy
                show fern_proud
                f proud "I'd have thought you were too cool for pop punk."
                j angry "Just because I like to stay focused on the job doesn't mean I can't have fun."
                hide fern_proud
                show fern_happy
                f happy "We should totally go see them! Invite your flatmate!"
                j annoyed "Err...we could go see them. Just us though."
                hide fern_happy
                show fern_thinking
                f thinking "Oh? What about the flatmate? Did you fall out? Did they mysteriously disappear? Did they run away to join the circus?"
                j annoyed "Can we just focus on the case please?"
                hide fern_thinking
                show fern_base
                f base "Of course, January doesn't want to tell me about herself. What a surprise."
                j annoyed "..."
                hide fern_base with Dissolve(0.25)
                jump .look_at

            "Wall Posts":
                show fern_thinking with Dissolve(0.25)
                f thinking "Nothing much for Ross and Ralph...Rich has some conversations going on though."
                f thinking "It mostly seems to be about hair dye?"
                f thinking "He's posting pics of his crazy colored hair and talking to some people about how great a brand his dye is."
                hide fern_thinking
                show fern_angry
                f angry "No! He's doing multi-level marketing! Richard, I'm disappointed in you."
                j annoyed "What's that?"
                f angry "Its a pyramid scheme where people shill terrible products. The real profits come from the sellers buying the product."
                j annoyed "Oh, I think a bunch of girls I went to high school with do that - like those posts on fakebook where they talk about their 'amazing results'?"
                f angry "That's it! They should just get real jobs! Like stalking people on fakebook to find out who's cheating!"
                j annoyed "..."
                hide fern_angry with Dissolve(0.25)
                jump .look_at

            "Interests":
                show fern_thinking with Dissolve(0.25)
                j thinking "their interests."
                f happy "Interests coming right up!"
                f thinking "So Ralph is interested in technology, stocks and space. Wow, I bet he's a Dusk fanboy. Gross."
                f thinking "Richard likes fashion, hair and music. He's not filled in much here, its kinda basic."
                f thinking "And Ross is the same. It only lists music and skateboarding."
                hide fern_thinking with Dissolve(0.25)
                jump .look_at

            "Nothing Else":
                label .question_3:
                    $ health.set_preview(1)
                    menu:
                        j "Okay! This info lets me eliminate one of the profiles. I can get rid of"

                        "Ralph":
                            hide screen case_notes
                            "No...I don't have any evidence that rules Ralph out. I need to think again."
                            $ health.fail("fakebook_puzzle.question_3")

                        "Ross":
                            hide screen case_notes
                            "No...I don't have any evidence that rules Ross out. I need to think again."
                            $ health.fail("fakebook_puzzle.question_3")

                        "Richard":
                            $ health.hide_preview()
                            "Richard. It wasn't him - a guy with brightly dyed hair can't be described as non-descript! And we were certain that that Blaize guy wasn't who Trevor met with."
                label .question_4:
                    $ health.set_preview(1)
                    menu:
                        j "Okay, down to two. We've established who fits the description, but who could have actually been at the hotel? I need more info on"

                        "Space":
                            j thinking "Ralph is interested in space, maybe there's something there?"
                            f thinking "I mean, space is a pretty big topic. What do you want me to look up?"
                            j thinking "Err. I dunno. Maybe that's not it."
                            hide screen case_notes
                            j angry "I don't know!"
                            $ health.fail("fakebook_puzzle.question_4")

                        "Concert":
                            $ health.hide_preview()
                            j proud "The post about the hair dye shows that it wasn't Richard - I'd hardly call bright colored hair 'non-descript'"
                            j proud "That leaves Ross and Ralph."
                            j proud "Ross seems to have gone to a concert recently. If my hunch is correct, that would give him an alibi for the time that Trevor met someone at the hotel."
                            hide fern_thinking
                            show fern_base
                            f base "Looking it up now..."
                            hide fern_base
                            show fern_very_happy
                            f vhappy "Ah hah!"
                            f vhappy "The concert started at 7pm last night! There's no way Ross could have been at the hotel!"
                            j proud "Gotcha. Ralph Smith...what were you doing with Trevor at the Billford Hotel?"
                            f vhappy "We're gonna find out!"
                            j proud "Yes we are."

                        "Hair Dye":
                            j thinking "This hair dye company. You said it was shady right?"
                            f thinking "Well MLMs are shady, but not illegal, are you sure this will help?"
                            hide screen case_notes
                            "Fern spent a deal of time investigating the hair dye company, but we never found his profile."
                            $ health.fail("fakebook_puzzle.question_4")

    hide fern_very_happy
    hide bg office afternoon
