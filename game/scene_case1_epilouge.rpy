label case1_epilouge:
    show bg office evening with Dissolve(1.0)
    "The next day was spent doing paperwork, billing and avoiding talking about the fight we almost had last night."
    show fern_proud with Dissolve(0.25)
    f proud "Let's go out. Celebrate another case cracked."
    j annoyed "I just want to go home, Fern. I'm tired."
    hide fern_proud
    show fern_angry
    f angry "Jan!"
    f angry "You have spent this whole case grumping at me and hiding things from me!"
    f angry "What happened to the old days? Celebrating after cases?"
    hide fern_angry
    show fern_proud
    f proud "1am trips to Toscos where you'd ride me around in a trolley?"
    f proud "That time we accidently stole that dog?"
    hide fern_proud
    show fern_angry
    f angry "I thought we were friends Jan!"

    j pain "We are friends, Fern."
    j proud "I miss Rover."
    j annoyed "I guess I thought we could get away with that sort of thing forever, that the past wouldn't catch up with me."
    j happy "Running this place with you was like being at uni again, it was a lark but we were making money, things were good."
    j annoyed "I'm sorry I let my own problems interfere with that."
    hide fern_angry
    show fern_happy
    j proud "We'll manage. Even without the sarge sending us cases."
    j proud "I mean, this town's full of scumbags."
    hide fern_happy
    show fern_proud
    f proud "Including us."
    j happy "Haha, yeah."
    j happy "So, where are we going?"
    hide fern_proud
    show fern_thinking
    f thinking "You'll come out?"
    j happy "Yeah, I clearly need a night with my best friend."
    hide fern_thinking
    show fern_very_happy
    f vhappy "YES! Let's go! Akiba Disuko!"
    j angry "Wait, what."
    j angry "Is that the thing ran by the uni's anime club?"
    j angry "Did you just call our friendship into question so you could blackmail me into going to your weeb disco?"
    hide fern_very_happy
    show fern_calm
    f calm "...yup."
    j happy "I fucking hate you."
    hide fern_calm with Dissolve(0.25)

    "An hour later, we met back up at the office, dressed to the nines."
    show fern_max_very_happy with Dissolve(0.25)
    j angry "What are you wearing?!"
    f max_vhappy "I'm Max Spirit! Raven Wrong's psychic assistant!"
    hide fern_max_very_happy
    show fern_max_happy
    f max_happy "Where's your costume?"
    j annoyed "Woah, I am not dressing up."
    j annoyed "Just going is a stretch."
    f max_happy "Carol's Costumes is still open, c'mon, we're going."
    hide fern_max_happy with Dissolve(0.25)

    "When we got back to the office, cosplay in hand, I saw the last thing I wanted to see."
    $ audio_crossFade(1.5, "audio/roulette.mp3")
    show avery_thinking with Dissolve(0.25)
    "Avery Jones. Ace detective, regular pain in my ass."
    "The very person I'd spent the last year running from."

    a thinking "The first time I see you in over a year and you're hanging out with children?"
    f max_angry "Hey! I'm twenty three years old!"
    hide avery_thinking
    show avery_proud
    a proud "Well you dress like you're thirteen."
    hide avery_proud
    show avery_base
    a base "Why was a deactivated warrant card used to get information from the DVLA, ex-constable Edwards?"
    "And that, was when things got complicated."
    $ renpy.hide_screen("case_notes")
    $ renpy.hide_screen("clock")
    hide avery_base with Dissolve(0.25)
    hide bg office evening with Dissolve(1.0)
    stop music1 fadeout 2.0
    "To Be Continued..."

    $ MainMenu(confirm=False)()
