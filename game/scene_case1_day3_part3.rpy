label case1_day3_part3:
    $ audio_crossFade(1.5, "audio/spy.mp3")
    show bg house night with Dissolve(1.0)
    "That night I headed to the address I'd been given."
    "The plan had been some light reconnaissance, but two hours in I hit the jackpot."
    "Who else had pulled up outside the house but Trevor Martin?"

    show trev_sad with Dissolve(0.25)
    "He looked even more nervous than he had outside the hotel."
    "He rang the doorbell and was quickly pulled inside. I wasn't able to get a view of whoever answered."
    hide trev_sad with Dissolve(0.25)
    "Just like before, he was in and out in less than ten minutes."
    show trev_sad with Dissolve(0.25)
    "This time however, he was holding a clear plastic folder."
    hide trev_sad with Dissolve(0.25)
    show document usb
    "Zooming in on my camera, I saw some documents and a usb drive."
    "We needed to see what was on that thing."

    j thinking "Hi, Louise? Its Jan from the agency."
    l sad "Do you have more news?"
    j thinking "Big news. Your husband has just visited the house of the man he's been meeting with."
    j thinking "We have a name and an address, but more importantly, I just saw him leave with some documents and a usb."
    j thinking "We need to see what's on that. I know its a big ask, but is there any chance you can get it for us?"
    l happy "I did it before, I can do it again. I'll call you as soon as I can."
    j thinking "Thanks, we're close, I can feel it. Speak soon!"
    l happy "Bye!"

    hide document usb
    hide bg house night with Dissolve(1.0)
