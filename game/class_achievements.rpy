init python:
    class achievements():
        def __init___(self):
            pass

        def grant_achievement(self, achievement):
            if achievement in persistent.achievements_list_1:
                persistent.achievements_list_1[achievement][0] = True

            elif achievement in persistent.achievements_list_2:
                persistent.achievements_list_2[achievement][0] = True

            elif achievement in persistent.achievements_list_3:
                persistent.achievements_list_3[achievement][0] = True
