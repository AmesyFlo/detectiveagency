init python:

    def callback(event, **kwargs):

        if event == "show":

            renpy.music.play("audio/beep.wav", channel="sound", loop=True)

        elif event == "slow_done" or event == "end":

            renpy.music.stop(channel="sound")

    def beepy_voice(event, interact=True, **kwargs):
        if not interact:
            return

        if event == "show_done":
            renpy.sound.play("audio/beep.wav", loop=True)
        elif event == "slow_done":
            renpy.sound.stop()
