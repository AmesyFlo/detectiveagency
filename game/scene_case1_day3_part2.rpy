label case1_day3_part2:

    define o = Character("Phone Operator")

    show bg office afternoon
    $ audio_crossFade(1.5, "audio/office.mp3")
    show fern_thinking


    f thinking "So what's the plan now?"
    j thinking "Well, I still need to follow up on the minibus I saw outside the warehouse."
    j thinking "With any luck its registered in Ralph's name."

    hide fern_thinking
    show fern_happy
    f happy "Ooh, getting our social engineering on! What's the pretext?"
    hide fern_happy with Dissolve(0.25)
    $ audio_crossFade(1.5, "audio/mystery.mp3")

    menu:
        "I'll pretend to"

        "Be His Boss":
            show fern_happy with Dissolve(0.25)
            f happy "Oooh, everyone loves to give information to an authority figure."
            j thinking "Exactly."
            j thinking "Go in completely sure of yourself, like your entitled to it, they give it."
            j thinking "Right, I'm going to get on the phone, keep quiet."
            hide fern_happy with Dissolve(0.25)

            o "Good Afternoon, this is Dan speaking, how can I help you today?"
            j thinking "Hi there, I'm looking to get the V5C records for a particular vehicle."
            o "Sure thing, before we go ahead can I just check that you're the vehicle's registered keeper?"
            j thinking "Actually, that's the issue."
            j thinking "An employee of mine has a bus registered in his name and we can't find him or the vehicle."
            j thinking "We're all very worried down here at...Northpool Green Transport Services."
            j thinking "If you could give us the information you have in the logs that would really help us find him."
            o "..."
            o "Well I'm terribly sorry to hear about your employee, but our records shouldn't contain any information that you don't already have."
            o "Surley your HR department can help you with this?"
            o "If you're really concerned I'd suggest trying the police."

            "It was not going well. I had to change tactic. The phone operator was suspicious."

            $ health.set_preview(4)
            menu:

                "Tell the Truth":
                    j thinking "I might as well come clean."
                    j thinking "I'm a private investigator. I work within the remit of the police."
                    j thinking "I can't tell you the details of my investigation due to client confidentiality, but knowing who owns this bus could really help someone."
                    o "Wow. Okay I definitely can't give you that information."
                    o "I'm sorry, you'll have to find him another way."
                    o "Is there anything else I can help you with today?"
                    "Seriously?"
                    j "Nothing, thanks."
                    $ health.insta_fail()

                "Pretend to be Police":
                    $ health.hide_preview()
                    j thinking "I might as well come clean."
                    j thinking "As this is connected to a sensitive ongoing case we were trying to keep it under wraps, but I'm with the police."
                    j thinking "We believe finding the owner of this bus will assist us in our investigation."
                    o "If you're with the police, why didn't you say that in the first place?"
                    jump case1_day3_part2.police

                "Hang Up":
                    j "Okay, I'll have him phone back. Sorry."
                    $ health.insta_fail()

        "Be His Wife":
            show fern_happy with Dissolve(0.25)
            f happy "Old school. You reckon it'll work?"
            j thinking "If I get someone older on the phone yeah. They're more likely to think that a couple should handle each other's business."
            j thinking "If its someone younger I might pivot."
            j thinking "Right, I'm going to get on the phone, keep quiet."
            hide fern_happy with Dissolve(0.25)

            o "Good Afternoon, this is Dan speaking, how can I help you today?"
            j thinking "Hi there, I'm looking to get the V5C records for our car."
            o "Sure thing, can I have your name please?"
            j thinking "Um, well my name is June, but I think the car is registered in my husband's name?"
            j thinking "That's Ralph Smith."
            o "I'm sorry June, only the registered keeper of the vehicle can access these records, you'll have to get your husband to call us."
            j thinking "I know but, can't you help me out just this once? Ralph's really busy and..."
            j thinking "We're concerned we bought a stolen car."
            o "Okay June, do you have a pen handy?"
            j thinking "Er, yes?"
            o "Well, if you go online to www dot gov dot uk slash checks when buying a used car - there's a dash between each of those words -
                you can see how to check if your vehicle's been stolen, okay?"
            j thinking "Oh, everything's online these days isn't it? I'm really not good at technology, um, could you check for me?"
            j thinking "Or just send me the V5C?"
            o "..."
            o "Mrs. Smith, our records don't contain any information that you, as his wife, wouldn't already know."
            o "If your husband wants these records, you'll need to ask him to phone himself. I'm sorry."
            o "The website really isn't that difficult to use."

            "It was not going well. I had to change tactic. If I pushed any harder he was going to get suspicious."

            $ health.set_preview(4)
            menu:

                "Tell the Truth":
                    j thinking "I might as well come clean."
                    j thinking "I'm a private investigator. I work within the remit of the police."
                    j thinking "I can't tell you the details of my investigation due to client confidentiality, but knowing who owns this bus could really help someone."
                    o "Wow. Okay I definitely can't give you that information."
                    o "I'm sorry, you'll have to find him another way."
                    o "Is there anything else I can help you with today?"
                    "Seriously?"
                    j "Nothing, thanks."
                    $ health.insta_fail()

                "Pretend to be Police":
                    $ health.hide_preview()
                    j thinking "I might as well come clean."
                    j thinking "As this is connected to a sensitive ongoing case we were trying to keep it under wraps, but I'm with the police."
                    j thinking "We believe finding the owner of this bus will assist us in our investigation."
                    o "If you're with the police, why didn't you say that in the first place?"
                    jump case1_day3_part2.police

                "Hang Up":
                    j "Okay, I'll have him phone back. Sorry."
                    $ health.insta_fail()


        "Be The Police":
            show fern_base with Dissolve(0.25)
            f base "That's risky, are you sure?"
            j annoyed "The DVLA don't give information out to just anyone. They have to comply with the authorities though."
            j annoyed "Without the sarge to protect us, I'll just have to make sure I'm as believable as possible."
            f thinking "And if you have to use your secret weapon?"
            j thinking "We'll see. I can't risk them tracing it back."
            j thinking "I'm just going to have to try."
            hide fern_base with Dissolve(0.25)

            o "Good Afternoon, this is Dan speaking, how can I help you today?"
            j thinking "Afternoon, this is Constable Edwards from Northpool Green Police Station."
            j thinking "I need the V5C records and any additional information you can provide on a minibus with the registration  BD5I SMR."
            jump case1_day3_part2.police

    label .police:
        o "I'm sorry officer, but don't the police already have access to this sort of information?"
        j thinking "We're concerned that our database might not be accurate, this could really help with an ongoing investigation, Dan."
        o "Two seconds, I think we've got a procedure for this."
        "Just my luck, someone good at operational security. Why did I have to get the component ones?!"
        o "Okay, so I can give you that information if you give your full name and warrant card number."
        "My secret weapon - my old warrant card, gathering dust at the bottom of a locked drawer."

        $ health.set_preview(4)
        menu:
            "Did I really want to use it?"

            "Yes":
                $ health.hide_preview()
                "I decided to use the warrant card."
                "It was 'proof' I was a police officer, and could get me places I was otherwise denied access to as a civilian investigator."
                "There was always the risk of it being traced back to the station, but that was one I was willing to take."
                "The case was getting too interesting, I didn't want to let Louise down and..."
                "Well, somewhere, in the back of my mind, the apprehension that Avery Jones would hear about this was quickly turning to excitement."
                "Not that I'd ever admit it."

                show document warrant

                j annoyed "Of course."
                j annoyed "Its Constable January Edwards, warrant number 53850-37383."
                o "Thanks. Can I take an email address to forward this to? Or did you want a paper copy?"
                j thinking "Actually, is there any chance you can just tell me the keeper's current address?"
                j thinking "I'll have our research assistant get in touch to get the full documents later."

                hide document warrant

                "I came away from the phone call with Ralph Smith's current address."
                "It was a close one, and I knew that using the warrant card wouldn't be without repercussions, but we had nearly cracked the case."

            "No":
                "No, I couldn't risk it. The card could be traced back with no issues, and then I'd have Avery Jones knocking at my door."
                "That's the last thing I wanted."
                $ health.insta_fail()

    hide office afternoon with Dissolve(1.0)
