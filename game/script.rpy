label init_variables:
    python:
        #achivements
        if persistent.achievements_list_1 is None : persistent.achievements_list_1 = {"case_1_1": [False, "cg placeholder 1", "btn placeholder 1"], "case_1_ending": [False, "cg placeholder 3", "btn placeholder 3"]}
        if persistent.achievements_list_2 is None : persistent.achievements_list_2 = {}
        if persistent.achievements_list_3 is None : persistent.achievements_list_3 = {}

        #case notes
        if not hasattr(renpy.store, 'case_dict') : case_dict = {}

        #health
        if not hasattr(renpy.store, 'health') : health = Health()

    return

# The game starts here.

label start:

    #characters
    define f = Character("Fern Anderson", image = "fern", callback = beepy_voice)
    image side fern base = "Fern/side_fern_base.png"
    image side fern angry = "Fern/side_fern_angry.png"
    image side fern happy = "Fern/side_fern_happy.png"
    image side fern vhappy = "Fern/side_fern_very_happy.png"
    image side fern calm = "Fern/side_fern_calm.png"
    image side fern proud = "Fern/side_fern_proud.png"
    image side fern sad = "Fern/side_fern_sad.png"
    image side fern thinking = "Fern/side_fern_thinking.png"
    image side fern max_happy = "Fern/side_fern_max_happy.png"
    image side fern max_angry = "Fern/side_fern_max_angry.png"
    image side fern max_vhappy = "Fern/side_fern_max_very_happy.png"

    define j = Character("January Edwards", image = "jan")
    image side jan angry = "Jan/side_jan_angry.png"
    image side jan annoyed = "Jan/side_jan_annoyed.png"
    image side jan happy = "Jan/side_jan_happy.png"
    image side jan pain = "Jan/side_jan_pain.png"
    image side jan proud = "Jan/side_jan_proud.png"
    image side jan thinking = "Jan/side_jan_thinking.png"

    define l = Character("Louise Martin", image = "lou")
    image side lou base = "Louise/side_lou_base.png"
    image side lou angry = "Louise/side_lou_angry.png"
    image side lou sad = "Louise/side_lou_sad.png"
    image side lou happy = "Louise/side_lou_happy.png"

    define t = Character("Trevor Martin", image = "trev")
    image side trev pain = "Trev/side_trev_pained.png"
    image side trev sad = "Trev/side_trev_sad.png"
    image side trev base = "Trev/side_trev_base.png"
    image side trev angry = "Trev/side_trev_angry.png"

    define b = Character("Blaize Praxis", image = "blaize")
    image side blaize base = "Blaize/side_blaize_base.png"
    image side blaize sad = "Blaize/side_blaize_sad.png"
    image side blaize angry = "Blaize/side_blaize_sigh.png"
    image side blaize happy = "Blaize/side_blaize_happy.png"

    define r = Character("Ralph Smith", image = "ralph")
    image side ralph base = "Ralph/side_ralph_base.png"
    image side ralph sad = "Ralph/side_ralph_sad.png"
    image side ralph angry = "Ralph/side_ralph_angry.png"

    define a = Character("Avery Jones", image = "avery")
    image side avery angry = "Avery/side_avery_angry.png"
    image side avery sad= "Avery/side_avery_sad.png"
    image side avery happy = "Avery/side_avery_happy.png"
    image side avery base = "Avery/side_avery_base.png"
    image side avery proud = "Avery/side_avery_proud.png"
    image side avery thinking = "Avery/side_avery_thinking.png"

    #audio
    define audio.mystery = "audio/mystery.mp3"
    define audio.office = "audio/office.mp3"
    define audio.swoosh = "audio/swoosh.mp3"

    #initialise variables
    $ renpy.call_in_new_context("init_variables")

    #dummy achivements
    $ achievements().grant_achievement("case_1_1")
    $ achievements().grant_achievement("case_1_ending")

    show screen clock()
    show screen case_notes()
    show screen update_vars()
#    show screen health()

    call dialog_test
    call tutorial from _call_tutorial
    call case1_intro
    call case1_day1

    call case1_day2
    call case1_day2_part2

    call case1_day3_part1
    call fakebook_puzzle
    call case1_day3_part2
    call case1_day3_part3
    call case1_day3_part4

    call case1_epilouge

    return

label game_over:
    "The trail went cold after that."
    "Eventually we had to conclude that this couldn't be cracked."
    "Mrs. Martin wasn't very happy, and bombed us with bad reviews."
    "Things were not looking good for the business..."
    "GAME OVER"
    $ MainMenu(confirm=False)()


label after_load:

    #initialise variables
    $ renpy.call_in_new_context("init_variables")
    return
