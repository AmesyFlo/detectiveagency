label case1_intro:

    show bg office afternoon
    show fern_thinking
    f thinking "What about the infidelity case that woman called about earlier?"
    j angry "Ugh. Do I have to?"
    hide fern_thinking
    show fern_base
    f "Do you want to pay the rent?"
    j "..."
    f "You can't always wait for cases from the Sarge. At some point the police are going to start doing their own jobs!"
    j "I guess that point is now. He hasn't called in weeks."
    j "I'll take the infidelity case, get her on the phone."
    hide fern_base
    show fern_very_happy
    j "Yes ma'am!"

    hide fern_very_happy with Dissolve(0.25)
    show bg office evening with Dissolve(1.0)
    $ audio_crossFade(1.5, "audio/brunch.mp3")

    show fern_thinking at left with Dissolve(0.25)
    show lou_base at center with Dissolve(0.25)

    "A few hours later, the client met us in the office."

    l base "Thanks for taking my case."

    hide lou_base
    show lou_sad at center
    l sad "I don't want it to be true but...I need to know."

    j thinking "I understand. With any luck, we find nothing."
    hide fern_thinking
    show fern_very_happy at left
    f vhappy "But if he is cheating, we can get enough evidence that you can take him to the cleaners!"
    j angry "Fern!"
    j angry "I can handle this. Go make Mrs. Martin a cup of tea."
    hide fern_very_happy
    show fern_sad at left
    f sad "S-sorry. Comin' right up!"
    hide fern_sad with Dissolve(0.25)

    j annoyed "Sorry about her. New hire. She's not quite grasped the art of talking to clients yet."
    j thinking "Can I ask you some questions?"

    hide lou_sad
    show lou_base
    $ work = False
    $ hubs = False
    l base "Go ahead."

    label .interview:
        menu:

            "Private Investigation":
                j  thinking "Can you describe why you decided to contact us?"
                hide lou_base
                show lou_sad
                l sad "Well, my husband has started acting odd of late."
                hide lou_sad
                show lou_base
                l base "He's been coming home late and making flimsy excuses about where he's been."
                l base "He's pretty high up at Fiscalworks so coming home late isn't new, but sneaking around is."
                j thinking "I see. That sounds awful Mrs. Martin."
                $ hubs = True
                hide lou_base with Dissolve(0.25)
                jump .interview

            "Louise":
                j thinking "How are you doing today?"
                l angry "Not great, considering I think my husband might be cheating on me!"
                "Fair. That was a case of foot in mouth to rival Fern..."
                l sad "Sorry. This is a horrible situation, and I need to get back to work soon."
                $ work = True
                jump .interview

            "Work" if work is True:
                    j thinking "If you don't mind me asking - where do you work?"
                    l base "I'm Director of Finance at MoneyStream."
                    j thinking "That sounds important, no wonder you're in a hurry."
                    l base "Heh. Not as important as you might think."
                    l base "MoneyStream is small, and failing. I'm hoping to use my connections and experience to jump ship to Fiscalworks soon."
                    l base "Trevor - my husband - is supposed to be helping me get a job there, but if he's cheating on me..."
                    jump .interview

            "Affair" if hubs is True:
                    j thinking "Do you have any suspicions as to who your husband might be seeing or where he might be going?"
                    hide lou_base
                    show lou_angry
                    l angry "That's why I came to you - I have no idea! Trev isn't like those other execs at his firm. He doesn't hire glamorous secretaries or spend half his day at 'working lunches'."
                    hide lou_angry
                    show lou_sad
                    l sad "He's a good man. Or so I thought."
                    j happy "Don't worry. We'll find out what's going on with him."
                    hide lou_sad with Dissolve(0.25)
                    jump .interview

            "That's All":
                stop music fadeout 3.0
                $ audio_crossFade(1.5, "audio/figure.mp3")

                hide lou_base with Dissolve(0.25)
                j thinking "If you can give me some basic details on your husband we can get started with surveillance straight away..."
                "With the interview over and the target's information in hand, we were on the tail of a new case."
                $ case_dict["Name"] = "- Target = Trevor Martin, an exec at Fiscalworks"
                $ case_dict["Car"] = "- His car reg is BX96 UI0"
                $ case_dict["Address"] = "- His address is 16 Prince St..."

                "Bummer it wasn't something more interesting, but PI's who are barley making rent can't be choosers."
