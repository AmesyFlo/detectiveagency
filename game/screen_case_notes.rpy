init offset = -1

screen case_notes():

    window align (0.9745, 0.05) xmaximum 240 ymaximum 471 background None:
        image "ui/phone ui.png"
        button:
            align (0.4, 0.47)
            action ToggleScreen("case_notes")
        window align (0.4, 0.47) xmaximum 180 ymaximum 400 background None: #32 #fixed
        #    has fixed:
        #        yfit False
        #        align (0.4, 0.47)
            vbox:
                spacing 8
                for k, v in reversed(case_dict.items()):
                    text v color "#000000" size 8

screen clock():
        window align (0.9745, 0.05) xmaximum 240 ymaximum 471 background None:
            image health.get_image()
            image health.get_preview_image()
            button:
                align (0.4, 0.47)
                action ToggleScreen("case_notes")
