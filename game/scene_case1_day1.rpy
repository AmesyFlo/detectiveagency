label case1_day1:
        $ audio_crossFade(1.5, "audio/spy.mp3")
        show bg hotel2 with Dissolve(1.0)
        "The first day of that investigation started out like any other infidelity case."
        "Boring."
        "I cased out the house, the office. Simple stuff."
        "Street parking was common on their street, and there was an overspill car park at his office, with no security."
        "Observation was going to be a piece of cake."
        "My only concern was being spotted by a well meaning member of the public."
        "Normally I'd call ahead to the sarge and let him know where I was running ops, but he wasn't answering his phone."

        show trev_sad with Dissolve(0.25)
        "On his way home from work, Trevor turned into a side street, then pulled up outside a dingy hotel."
        "Perfect. Day one, and I was gonna catch him in the act."
        "Easy. Money."
        $ case_dict["Hotel"] = "- He went to the Billford Hotel after work."

        "He disappeared inside."
        hide trev_sad with Dissolve(0.25)
        "They probably wouldn't leave at the same time, but if I waited here a while, I was bound to get a photo of whoever he was meeting."

        j proud "Hey Fern! Check the Billford Hotel against the list will ya?"
        f happy "You've spotted him already? Nice one! Over."
        j annoyed "You don't need to say over, we're on the phone."
        f happy "Nope - not here. The Billford Hotel isn't known to be a brothel, crack house or other den of iniquity! At least to us."
        $ case_dict["Hotel"] = "- He went to the Billford Hotel after work, which isn't known for harboring illegal activities."
        f happy "Also, I do need to say over, because it's cool. Over."
        j annoyed "How are you even an adult?"
        j proud "But, thanks. Looks like this could be an easy one."
        j angry "Shit! Gotta go - I see someone."

        show trev_sad with Dissolve(0.25)
        "It was Trev."
        hide trev_sad
        show trev_angry
        "He'd only been in there ten minutes. What had happened? Was I burned? But why not just go home if that was the case?"
        "He got in his car. I thought to follow, but my original plan had been to see who else came out of the Billford."
        hide trev_angry with Dissolve(0.25)
        $ health.set_preview(4)
        menu:
            "In the end I"

            "Followed Trevor":
                "I followed Trevor, but he went straight home."
                $ health.insta_fail()

            "Continued survailling the hotel":
                $ health.hide_preview()
                "I continued the surveillance, hoping I'd see something interesting."
                "I was there for about an hour before another man came out of the hotel."
                show blaize_sad with Dissolve(0.25)
                "He was distinctive, to say the least. I shot a quick photo of him."
                hide blaize_sad
                show blaize_sigh
                "I managed to get his face. He looked concerned, stressed out."
                $ case_dict["Blaize"] = "- A brightly dressed, purple haired man left the hotel after Trevor."
                hide blaize_sigh with Dissolve(0.25)

                "Another hour after that, a third man emerged."
                show ralph_base with Dissolve(0.25)
                "Head down, in a matching dark hoodie and trackies, he looked straight at the pavement as he walked."
                "As if he was concerned about surveillance - I'd seen this type before."
                "I grabbed a photo of him too."
                $ case_dict["Ralph"] = "- Another man left after both of them, dressed in dark clothing and hiding his face."
                hide ralph_base with Dissolve(0.25)

                j thinking "Hey. I'm going to run observation the rest of the night."
                j thinking "I'm sending you some photos. Can you see what you can find on these two?"
                f happy "Sure thing! Hacker extroidanaire at your service! Over."
                j annoyed "Fern, I mean like, check Fakebook and shit."
                f sad "I know, I know! I won't do anything dodgy."
                f proud "A photo is not nearly enough info to do anything dodgy with! Over."

                "Sometimes I wondered why I'd employed Fern."
                "She wound me up like no man's business."
                "She was good at what she did though - that girl could find anything with a keyboard in hand."
                "She deserved a chance - one that the startups and tech giants wouldn't give her."

                "I waited most of the night, but no one else came or left."
                "Eventually I gave up, resolving to figure out who these two were tomorrow."

                stop music fadeout 3.0
                $ health.reset_health()
