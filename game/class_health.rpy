init python:
    class Health:
        def __init__(self):
            self.max = 4
            self.preview = False
            self.current = self.max
            self.preview_amount = 0

        def increase_health(self, amount):
            if self.current < self.max:
                self.current += amount

        def decrease_health(self, amount):
            if self.current > 0:
                self.current -= amount
                if self.current <= 0:
                    renpy.jump("game_over")

        def decrease_health_tutorial(self, amount):
            if self.current > 0:
                self.current -= amount
                if self.current <= 0:
                    renpy.jump("tutorial_fail")

        def get_image(self):
            if self.current == 4:
                return "ui/phone ui clock 9.png"
            elif self.current == 3:
                return "ui/phone ui clock 12.png"
            elif self.current == 2:
                return "ui/phone ui clock 3.png"
            elif self.current == 1:
                return "ui/phone ui clock 6.png"
            else:
                return "ui/phone ui clock full.png"

        def get_preview_image(self):
            if self.preview == True:
                if self.preview_amount == 4:
                    return "ui/phone ui clock preview 4.png"
                elif self.preview_amount == 3:
                    return "ui/phone ui clock preview 3.png"
                elif self.preview_amount == 2:
                    return "ui/phone ui clock preview 2.png"
                elif self.preview_amount == 1:
                    return "ui/phone ui clock preview 1.png"
            else:
                return "ui/blank.png"

        def set_preview(self, amount):
            renpy.hide_screen("case_notes")
            self.preview = True
            self.preview_amount = amount + abs(self.max - self.current)

        def fail(self, label_name):
            renpy.play("audio/swoosh.mp3")
            self.decrease_health(1)
            renpy.jump(label_name)
            self.preview = False

        def insta_fail(self):
            renpy.play("audio/swoosh.mp3")
            self.decrease_health(5)
            self.preview = False

        def fail_tutorial(self, label_name):
            renpy.play("audio/swoosh.mp3")
            self.decrease_health_tutorial(1)
            renpy.jump(label_name)
            self.preview = False

        def reset_health(self):
            renpy.play("audio/swoosh.mp3")
            self.preview = False
            self.current = self.max

        def hide_preview(self):
            self.preview = False
            self.preview_amount = 0
