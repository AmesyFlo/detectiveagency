label case1_day2:
    $ audio_crossFade(1.5, "audio/office.mp3")
    show bg office afternoon with Dissolve(1.0)

    show fern_happy with Dissolve(0.25)
    f happy "Mornin' Jan!"
    j proud "Hey."
    f happy "Guess what?"
    j annoyed "What?"
    hide fern_happy
    show fern_very_happy
    f vhappy "I know who you're purple haired weirdo is!"
    j proud "Already? Seriously? Ugh. You're good."
    j thinking "So?"
    hide fern_very_happy
    show fern_proud
    f proud "Well, he was pretty easy to find."
    f proud "A guy like that in a town like this?"
    f proud "The name's Blaize. Blaize Praxis."
    j happy "Blaize. Praxis. Hahahaha!"
    j thinking "That's not his real name right?"
    hide fern_proud
    show fern_happy
    f happy "Nah, but it doesn't matter. All his socials are connected to that. He's InstaPic famous. And a Twinge Streamer."
    $ case_dict["Blaize"] = "- Blaize Praxis, a social media celeb, left the hotel after Trevor."
    j annoyed "Ugh."
    j thinking "Seriously though, why would someone famous risk being seen somewhere like the Billford? He certainly wouldn't be staying there."

    hide fern_happy
    show fern_angry
    f angry "Wait..."
    menu:
        f "You don't think HE'S the other woman do you?"

        "No way.":
            f sad "Aw. That would have such a headline! InstaPic Star in Bed with Finance Husband?!"
            j proud "Err, that title could use some work."

            menu:
                j "But honestly, I think"

                "He's cheating with someone else":
                    j thinking "Nah. Too young. And outlandish. And he wouldn't have been so brazen if he was sneaking around."
                    $ case_dict["Blaize"] = "- Blaize Praxis, a social media celeb, left the hotel after Trevor. He's not the person we're looking for."
                    $ affair = True

                "There isn't another woman.":
                    j thinking "There isn't another woman at all."
                    j thinking "He wasn't in that hotel for long enough, and he wasn't acting like he knew he was being followed."
                    j thinking "He had other business there. Could be drugs? Some kind of deal? I'm not sure yet."
                    $ case_dict["Affair"] = "- I don't think Trevor is having an affair. His sneaking around is connected to something else."
                    $ affair = False

        "He could be.":
            j thinking "Could be. I mean, an InstaPic celeb and an investment banker? That would be quite the scandal."
            $ case_dict["Blaize"] = "- Blaize Praxis, a social media celeb, left the hotel after Trevor. He could be the man we're looking for."
            $ affair = True

    play sound door
    "Our theorizing was interrupted by Louise Martin bursting into the office."
    show lou_angry at left with Dissolve(0.25)
    l angry "Detective! I've got something you need to look at urgently!"
    "She pushed her phone into my hands."
    l angry "I managed to get  a quick look at his messages. He must have gone to this hotel last night! Did you follow him, did you-"
    j annoyed "Woah. Okay, Mrs. Martin. Uh, take a seat, I'll have a look at this."
    j thinking "I'd normally save this for the report, but yes, we did follow him to this hotel last night."
    l angry "I knew it! He IS having an affair, he-"
    if affair is False:
        j thinking "Hey. Calm down. I don't think he's having an affair."
        j thinking "Honestly, it doesn't look like he's wrapped up in anything good, but its something else."
    else:
        j thinking "I'm sorry. But we're going to get to the bottom of this, okay?"

    "I took a moment to rearrange my notes before looking at the evidence she'd brought me."
    $ del case_dict["Car"]
    $ del case_dict["Address"]

    show document phone
    hide lou_angry with Dissolve(0.25)
    hide fern_angry with Dissolve(0.25)
    "I took a look at the messages."
    "Trevor was asked to meet with a 'non-descript' man at the Billford Hotel last night."
    $ case_dict["Message"] = "- Trevor was asked to meet a 'non-descript' man at the hotel."
    "As I read over the message once more, I noticed that Louise's gaze kept drifting to her handbag."
    "She looked pensive, and was tapping her foot in what could have been a nervous manner."

    $ health.set_preview(4)
    menu:
        "Leave it be":
            "I didn't want to make a client feel any more uncomfortable than they had to be, but I had a bad feeling when she left the office."
            $ health.insta_fail()

        "Ask if she's okay":
            $ health.hide_preview()
            j thinking "Louise, is everything okay? You seem nervous."
            l angry "My husband might be cheating on me! Why wouldn't I be nervous?!"
            l angry "I could lose my career!"
            f thinking "You're more concerned about him getting you a job than your marriage? Stone cold!"
            j angry "Fern! Client. Relations."
            l base "...a-and my marriage of course!"
            f sad "..."
            j annoyed "..."
            l angry "Fine! I'm not worried about my marriage."
            "Suddenly, piece of paper was shoved into my hands."
            l sad "I found this."
            hide document phone
            show document payment
            l sad "I don't recognize the company at all."
            l sad "If it was linked to his work I'd know it."
            hide lou_sad with Dissolve(0.25)
            l angry "But these items? Bullshit. Totally meaningless."
            l sad "If you have money moving that's undocumented, that looks bad. But you can't have evidence of money that's been used for something it shouldn't have."
            l sad "So you make something like this."
            "The paper was a receipt of payment from a company called KnowLedger."
            "The company had sent Trev two thousand pounds."
            $ case_dict["Money"] = "- Trevor was sent 2k by KnowLedger."

            label .interview_2:
                menu:
                    "Payment":
                        f thinking "Why would a company he doesn't work for be sending him money?"
                        f thinking "Lou! Can I call you Lou? Does your hubs have a side hustle? Maybe he's got some crazy secret job!"
                        f happy "Like he's a stripper! Or an art thief! Or a spy!"

                        menu:
                            l "Could we speak just the two of us again?"

                            "Let Fern speak":
                                j thinking "Fern - say that again."
                                l angry "*gasp*"
                                f happy "Like he's a strip-"
                                j thinking "Just the last part."
                                f base "Or a spy?"
                                j proud "Thanks. I'm starting to put two and two together...or two and two thousand."
                                jump .interview_2

                            "Shut her up":
                                j angry "Fern - remember what I said earlier?"
                                f sad "Sorry..."
                                jump .interview_2

                    "KnowLedger":
                        j thinking "Who is KnowLedger?"
                        l base "I've never heard of them."
                        f thinking "..."
                        f thinking "..."
                        f thinking "..."
                        f thinking "Generic webpage, no listed employees on ConnectON, companies house only lists the initials of the owners."
                        j proud "A front."
                        f proud "A front."
                        l sad "...shit."
                        $ case_dict["Money"] = "- Trevor was sent 2k by KnowLedger, a front for another company or person."
                        jump .interview_2

                    "That's All":

                        hide document payment
                        show lou_sad at left with Dissolve(0.25)
                        show fern_happy with Dissolve(0.25)
                        j proud "Thanks for this Mrs. Martin. This could prove to be the key to where your husband's been going."
                        if affair is False:
                            j thinking "This pretty much confirms my theory that there's no affair."
                        j thinking "I know this is a lot to ask, but do you think you'd be capable of trying to get any more of his messages?"
                        hide fern_angry
                        show fern_very_happy
                        f vhappy "Oh! OH! I could bug his phone!"
                        j annoyed "Fern. We do not use illegal practices at our agency."
                        hide fern_very_happy
                        show fern_sad
                        j annoyed "If we obtained his messages that way, it wouldn't be of much help to Mrs. Martin's lawyers."
                        j annoyed "It would either be non-admissible evidence, or we'd be counter-sued."
                        j annoyed "So no. We will not be bugging his phone."
                        j proud "Mrs. Martin on the other hand, just happened to see the messages on his unlocked phone, in their home."
                        j thinking "Again, sorry about her."
                        j thinking "Just do what you can, okay?"
                        hide lou_sad
                        show lou_happy at left
                        l happy "I will! Thankyou!"
                        hide lou_happy with Dissolve(0.25)
                        "With that she rushed off, quick as she came."

                        j annoyed "Could you not talk about illegal shit in front of clients?"
                        f sad "Sorry. I just get carried away."
                        j thinking "I know. But it puts the agency at risk. We don't want that kind of reputation."
                        j thinking "I don't care what you do on your own time, but here, it can't be illegal. And if its grey-area, I don't want to know."
                        f sad "Yeah, yeah. Got it Boss."
                        j thinking "Anyway, this is some good info!"
                        hide fern_sad
                        show fern_happy
                        f happy "Yeah?"

                        $ audio_crossFade(1.5, "audio/mystery.mp3")

                        label .meeting:
                            $ health.set_preview(1)
                            menu:
                                j "I know who Trevor met with. It was"

                                "Blaize.":
                                    j thinking "Blaize. I've just got a gut feeling."
                                    j thinking "Why else would he have been at that hotel?"
                                    $ case_dict["Blaize"] = "- Blaize Praxis, a social media celeb, left the hotel after Trevor. He's the man Trevor met with."
                                    $ health.fail("case1_day2.meeting")

                                "The Other Man.":
                                    $ health.hide_preview()
                                    j thinking "The other man. The message said to meet someone 'non-descript'."
                                    j thinking "There is no way in hell Mr. Praxix could be described that way!"
                                    $ case_dict["Blaize"] = "- Blaize Praxis, a social media celeb, left the hotel after Trevor. He's not the man Trevor met with."
                                    $ case_dict["Ralph"] = "- Another man left after both of them, dressed in dark clothing and hiding his face. I'm certain this is who Trevor met with."
                                    f happy "You're right! Nice. What next?"
                                    j thinking "Well, this photo isn't a lot to go on. I'm going to have to do some more surveillance and see if I can figure out what they're up to."
                                    j thinking "I'll start with the address this company is registered to."
                                    j thinking "I know its a long shot, but do you think you can get anything online with just the photo?"
                                    f sad "I can try..."
                                    f thinking "Wait a sec! Does this mean we can rule out Blaize?"
                                    j thinking "Well, we saw him coming out of that hotel. He might be up to something, but I'm not convinced its related to us right now."
                                    $ del case_dict["Blaize"]
                                    j thinking "Just focus on our other mysterious visitor for now."
                                    hide fern_happy with Dissolve(0.25)

                                "Someone We Haven't Seen":
                                    j thinking "Someone we haven't seen yet. The message said to meet someone 'non-descript'."
                                    j thinking "I wouldn't describe either of the people we saw that way."
                                    $ case_dict["Blaize"] = "- Blaize Praxis, a social media celeb, left the hotel after Trevor. He's not the man Trevor met with."
                                    $ health.fail("case1_day2.meeting")

                        $ health.reset_health()
